package com.mini.authorizer.controller

import com.mini.authorizer.model.ApiResponse
import com.mini.authorizer.model.CartaoOperationResult
import com.mini.authorizer.model.Transacao
import com.mini.authorizer.service.TransacaoService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("transacoes")
class TransacaoController(
    private val transacaoService: TransacaoService
) {
    @PostMapping
    fun transacao(@RequestBody transacao: Transacao): ResponseEntity<ApiResponse> {
        return when(val result = transacaoService.transacao(transacao)) {
            is CartaoOperationResult.Success -> {
                ResponseEntity.status(HttpStatus.CREATED).build()
            }
            is CartaoOperationResult.Error -> {
                ResponseEntity.status(result.failure.statusCode).body(ApiResponse(error = result.failure))
            }
        }
    }
}