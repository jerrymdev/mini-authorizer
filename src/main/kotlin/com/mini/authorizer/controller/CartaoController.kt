package com.mini.authorizer.controller

import com.mini.authorizer.model.ApiResponse
import com.mini.authorizer.model.Cartao
import com.mini.authorizer.model.CartaoOperationResult
import com.mini.authorizer.service.CartaoService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.math.BigDecimal

@RestController
@RequestMapping("cartoes")
class CartaoController(private val cartaoService: CartaoService) {
    @PostMapping
    fun geraCartao(
        @RequestBody cartao: Cartao
    ): ResponseEntity<ApiResponse> {
        return when(val result = cartaoService.criarCartao(cartao)) {
            is CartaoOperationResult.CartaoAlreadyExistsSuccess -> {
                ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(ApiResponse(cartao = result.cartao))
            }
            is CartaoOperationResult.Success -> {
                ResponseEntity.status(HttpStatus.CREATED).body(ApiResponse(cartao = result.cartao))
            }
            is CartaoOperationResult.Error -> {
                ResponseEntity.status(result.failure.statusCode).body(ApiResponse(error = result.failure) )
            }
        }
    }

    @GetMapping("{numeroCartao}")
    fun saldo(@PathVariable numeroCartao: String): ResponseEntity<BigDecimal?> {
        return when( val result = cartaoService.buscarCartao(numeroCartao)) {
            is CartaoOperationResult.Success -> {
                ResponseEntity.status(HttpStatus.OK).body(result.cartao.saldo?.valor)
            }
            is CartaoOperationResult.Error -> {
                ResponseEntity.status(HttpStatus.NOT_FOUND).build<BigDecimal>()
            }
        }
    }

}