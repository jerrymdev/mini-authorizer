package com.mini.authorizer.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.redis.connection.RedisConnectionFactory
import org.springframework.data.redis.core.RedisTemplate
import java.util.UUID


@Configuration
class RedisConfiguration {
    @Bean
    fun redisTemplate(connectionFactory: RedisConnectionFactory?): RedisTemplate<UUID, String>? {
        val template: RedisTemplate<UUID, String> = RedisTemplate<UUID, String>()
        template.setConnectionFactory(connectionFactory!!)
        return template
    }
}