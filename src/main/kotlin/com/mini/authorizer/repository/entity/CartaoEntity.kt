package com.mini.authorizer.repository.entity

import com.mini.authorizer.model.Cartao
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name = "cartao")
data class CartaoEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    val id: Int? = null,

    @Column(unique = true)
    val numeroCartao: String? = null,

    @Column
    val senha: String? = null,

    @OneToOne(cascade = [CascadeType.ALL])
    @JoinColumn(name = "id_saldo", nullable = false)
    val saldo: SaldoCartaoEntity? = null
)

fun CartaoEntity.toModel() = Cartao(id, numeroCartao, senha, saldo?.toModel())
