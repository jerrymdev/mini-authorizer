package com.mini.authorizer.repository.entity

import com.mini.authorizer.model.SaldoCartao
import java.math.BigDecimal
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Column
import javax.persistence.FetchType
import javax.persistence.OneToOne
import javax.persistence.Table


@Entity
@Table(name = "saldo")
data class SaldoCartaoEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    val id: Int? = null,

    @Column(nullable = false)
    val valor: BigDecimal? = null,

    @OneToOne(mappedBy = "saldo", fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    val cartao: CartaoEntity? = null
)

fun SaldoCartaoEntity.toModel() = SaldoCartao(id = id, valor = valor, cartaoId = cartao?.id)
