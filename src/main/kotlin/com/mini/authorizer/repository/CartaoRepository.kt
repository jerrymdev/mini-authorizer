package com.mini.authorizer.repository

import com.mini.authorizer.repository.entity.CartaoEntity
import org.springframework.data.repository.CrudRepository

interface CartaoRepository: CrudRepository<CartaoEntity, Int> {
    fun findByNumeroCartao(numero: String): CartaoEntity?
}