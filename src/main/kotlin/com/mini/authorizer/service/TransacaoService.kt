package com.mini.authorizer.service

import com.mini.authorizer.failure.BusinessFailures
import com.mini.authorizer.failure.ServerFailures
import com.mini.authorizer.failure.exceptions.OperationAlreadyRunningException
import com.mini.authorizer.failure.exceptions.SaldoInsulficenteException
import com.mini.authorizer.failure.exceptions.SenhaIncorretaException
import com.mini.authorizer.model.CartaoOperationResult
import com.mini.authorizer.model.Transacao
import com.mini.authorizer.repository.entity.toModel
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.util.UUID

@Service
class TransacaoService(
    private val cartaoService: CartaoService,
    private val redisTemplate: RedisTemplate<String, String>
) {
    fun transacao(transacao: Transacao) = try {
        hasOperationRunning(transacao.numeroCartao)
        addOperationToCache(transacao.numeroCartao)
        when(val result = cartaoService.buscarCartao(transacao.numeroCartao)) {
            is CartaoOperationResult.Success -> {
                validaSenha(transacao.senha, result.cartao.senha!!)
                val novoSaldo = debitaSaldo(transacao.valor, result.cartao.saldo?.valor!!)
                val cartao = result.cartao.copy(saldo = result.cartao.saldo.copy(valor = novoSaldo))

                CartaoOperationResult.CartaoSuccess(cartao = cartaoService.atualizar(cartao).toModel())
            }
            is CartaoOperationResult.DatabaseError -> {
                CartaoOperationResult.BusinessError(BusinessFailures.cartaoInexistente(result.failure))
            }
            else -> result
        }
    } catch (ex: Exception ) {
        when(ex) {
            is SaldoInsulficenteException -> {
                CartaoOperationResult.BusinessError(BusinessFailures.saldoInsuficiente(ex))
            }
            is SenhaIncorretaException -> {
                CartaoOperationResult.BusinessError(BusinessFailures.senhaIncorreta(ex))
            }
            is OperationAlreadyRunningException -> {
                CartaoOperationResult.ServerError(ServerFailures.operationInExecution())
            }
            else -> {
                CartaoOperationResult.ServerError(ServerFailures.genericError(ex))
            }
        }
    } finally {
        removeOperationFromCache(transacao.numeroCartao)
    }

    fun debitaSaldo(valor: BigDecimal, saldoAtual: BigDecimal) = when(validaTransacao(valor, saldoAtual)) {
        true -> {
            saldoAtual.minus(valor)
        }
        false -> {
            throw SaldoInsulficenteException()
        }
    }

    private fun validaTransacao(valor: BigDecimal, saldoAtual: BigDecimal) = saldoAtual >= valor

    private fun validaSenha(senhaFornecida: String, senhaCartao: String) {
        when(senhaFornecida == senhaCartao) {
            true -> return
            false -> throw SenhaIncorretaException()
        }
    }

    private fun hasOperationRunning(numeroCartao: String) {
        return when(redisTemplate.opsForValue().get(numeroCartao)) {
            null -> {
                println("eee")
                return
            }
            else -> {
                println("tem exec")
                throw OperationAlreadyRunningException()
            }
        }
    }

    private fun addOperationToCache(numeroCartao: String) {
        try {
            redisTemplate.opsForValue().set(numeroCartao, numeroCartao)
        } catch (ex: Exception) {
            println(ex)
        }

    }

    private fun removeOperationFromCache(numeroCartao: String) {
        redisTemplate.opsForValue().getAndDelete(numeroCartao)
    }
}