package com.mini.authorizer.service

import com.mini.authorizer.failure.CartaoOperationFailure
import com.mini.authorizer.failure.DatabaseFailures
import com.mini.authorizer.failure.ServerFailures
import com.mini.authorizer.model.Cartao
import com.mini.authorizer.model.CartaoOperationResult
import com.mini.authorizer.model.toEntity
import com.mini.authorizer.repository.CartaoRepository
import com.mini.authorizer.repository.entity.CartaoEntity
import com.mini.authorizer.repository.entity.toModel
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.stereotype.Service
import java.math.BigDecimal

@Service
class CartaoService(
    private val cartaoRepository: CartaoRepository
) {
    fun criarCartao(cartao: Cartao): CartaoOperationResult = try {
        val result = cartaoRepository.save(cartao.toEntity()).toModel()
        CartaoOperationResult.CartaoSuccess(result)
    } catch (duplicateException: DataIntegrityViolationException) {
        buscarCartao(cartao.numeroCartao!!).onSuccess {
            CartaoOperationResult.CartaoAlreadyExistsSuccess(it.cartao)
        }
    } catch (ex: Exception) {
        CartaoOperationResult.ServerError(ServerFailures.genericError(ex))
    }

    fun  buscarCartao(numeroCartao: String) = try {
        when (val cartao =  cartaoRepository.findByNumeroCartao(numeroCartao)) {
            null -> CartaoOperationResult.DatabaseError(DatabaseFailures.notFoundRecord())
            else ->  CartaoOperationResult.CartaoSuccess(cartao = cartao.toModel())
        }
    } catch (ex: Exception) {
        CartaoOperationResult.ServerError(ServerFailures.genericError(ex))
    }

    fun atualizar(cartao: Cartao) = cartaoRepository.save(cartao.toEntity())
}