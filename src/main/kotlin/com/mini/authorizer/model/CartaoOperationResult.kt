package com.mini.authorizer.model

import com.mini.authorizer.failure.CartaoOperationFailure

sealed class CartaoOperationResult {
    abstract class Success(val cartao: Cartao) : CartaoOperationResult() {
        abstract fun copy(cart: Cartao): Success
    }

    class CartaoSuccess(cartao: Cartao): Success(cartao) {
        override fun copy(cart: Cartao): Success = CartaoSuccess(cartao)
    }

    class CartaoAlreadyExistsSuccess(cartao: Cartao): Success(cartao) {
        override fun copy(cart: Cartao): Success = CartaoSuccess(cartao)
    }



    abstract class Error(val failure: CartaoOperationFailure): CartaoOperationResult()
    class ServerError(failure: CartaoOperationFailure.ServerFailureException): Error(failure)
    class DatabaseError(failure: CartaoOperationFailure.DatabaseFailureException): Error(failure)

    class BusinessError(failure: CartaoOperationFailure.BusinessError): Error(failure)

    fun onSuccess(transform: (successOperation: Success) -> CartaoOperationResult): CartaoOperationResult {
        return when(this) {
            is Success -> transform(this)
            else -> this
        }
    }
}
