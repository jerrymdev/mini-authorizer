package com.mini.authorizer.model

import java.math.BigDecimal
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Column
import javax.persistence.OneToOne
import javax.persistence.Table

data class SaldoCartao(
    val id: Int? = null,
    val valor: BigDecimal? = BigDecimal(500.00),
    val cartaoId: Int? = null
)
