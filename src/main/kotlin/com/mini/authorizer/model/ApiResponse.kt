package com.mini.authorizer.model

import com.mini.authorizer.failure.CartaoOperationFailure
import com.mini.authorizer.failure.ErrorResponse
import com.mini.authorizer.model.Cartao
import java.math.BigDecimal

class ApiResponse(
    val cartao: Cartao? = null,
    val error: CartaoOperationFailure? = null
)
