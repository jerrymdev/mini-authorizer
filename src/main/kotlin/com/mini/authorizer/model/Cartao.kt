package com.mini.authorizer.model

import com.mini.authorizer.repository.entity.CartaoEntity
import com.mini.authorizer.repository.entity.SaldoCartaoEntity
import java.math.BigDecimal

data class Cartao(
    val id: Int?,
    val numeroCartao: String?,
    val senha: String?,
    val saldo: SaldoCartao? = SaldoCartao(valor = BigDecimal(500.00))
)

fun Cartao.toEntity() = CartaoEntity(id, numeroCartao, senha, saldo = SaldoCartaoEntity(valor = saldo?.valor))
