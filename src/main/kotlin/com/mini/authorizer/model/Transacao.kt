package com.mini.authorizer.model

import java.math.BigDecimal

data class Transacao(
    val numeroCartao: String,
    val senha: String,
    val valor: BigDecimal
    )
