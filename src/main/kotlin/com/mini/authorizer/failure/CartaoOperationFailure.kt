package com.mini.authorizer.failure

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.mini.authorizer.model.CartaoOperationResult

@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "type"
)
@JsonSubTypes(
    JsonSubTypes.Type(value = CartaoOperationResult.DatabaseError::class, name = "databaseFailure"),
    JsonSubTypes.Type(value = CartaoOperationResult.ServerError::class, name = "serverFailure")
)
@JsonIgnoreProperties(value = ["stackTrace", "localizedMessage", "internalReason"])
sealed class CartaoOperationFailure(
    val code: String,
    val statusCode: Int,
    val internalReason: Throwable? = null,
    override val message: String,
): RuntimeException(message) {
    class ServerFailureException(
        code: String,
        statusCode: Int,
        internalReason: Throwable? = null,
        message: String
    ) : CartaoOperationFailure(code, statusCode, internalReason, message)

    class DatabaseFailureException(
        code: String,
        statusCode: Int,
        internalReason: Throwable? = null,
        message: String
    ) : CartaoOperationFailure(code, statusCode, internalReason, message)

    class BusinessError(
        code: String,
        statusCode: Int,
        internalReason: Throwable? = null,
        message: String
    ) : CartaoOperationFailure(code, statusCode, internalReason, message)

    override fun toString(): String {
        return "[code=$code] - [message=$message] [statusCode=$statusCode] [internalReasonMessage=${internalReason?.message ?: "nothing"}]"
    }

    fun toErrorResponse() = ErrorResponse(code, message.trim())
}
