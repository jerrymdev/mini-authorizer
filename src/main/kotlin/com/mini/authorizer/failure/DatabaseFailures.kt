package com.mini.authorizer.failure

object DatabaseFailures {
    fun genericError(internalReason: Throwable) = CartaoOperationFailure.DatabaseFailureException(
        code = "222",
        statusCode = 500,
        internalReason = internalReason,
        message = "Generic error",
    )

    fun duplicatedRecord(internalReason: Throwable) = CartaoOperationFailure.DatabaseFailureException(
        code = "333",
        statusCode = 422,
        internalReason = internalReason,
        message = "Cartao ja existe",
    )

    fun notFoundRecord() = CartaoOperationFailure.DatabaseFailureException(
        code = "444",
        statusCode = 404,
        message = "Cartao nao existe",
    )
}