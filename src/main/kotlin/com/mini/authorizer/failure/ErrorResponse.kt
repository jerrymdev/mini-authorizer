package com.mini.authorizer.failure

data class ErrorResponse(
    val code: String,
    val message: String,
)
