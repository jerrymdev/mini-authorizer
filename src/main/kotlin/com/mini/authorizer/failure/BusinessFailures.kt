package com.mini.authorizer.failure

object BusinessFailures {
    fun saldoInsuficiente(internalReason: Throwable) = CartaoOperationFailure.BusinessError(
        code = "777",
        statusCode = 422,
        internalReason = internalReason,
        message = "saldo insuficiente",
    )

    fun senhaIncorreta(internalReason: Throwable) = CartaoOperationFailure.BusinessError(
        code = "888",
        statusCode = 422,
        internalReason = internalReason,
        message = "senha incorreta",
    )

    fun cartaoInexistente(internalReason: Throwable) = CartaoOperationFailure.BusinessError(
        code = "999",
        statusCode = 422,
        internalReason = internalReason,
        message = "cartao inexistente",
    )
}