package com.mini.authorizer.failure.exceptions

class OperationAlreadyRunningException: RuntimeException()