package com.mini.authorizer.failure

object ServerFailures {
    fun genericError(internalReason: Throwable) = CartaoOperationFailure.ServerFailureException(
        code = "111",
        statusCode = 500,
        internalReason = internalReason,
        message = "Generic server error",
    )

    fun operationInExecution() = CartaoOperationFailure.ServerFailureException(
        code = "9090",
        statusCode = 422,
        message = "Operacao em execucao, tente novamente",
    )
}